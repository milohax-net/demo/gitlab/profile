Files for dynamic GitLab profile demonstration

This is for the [first GitLab AU/NZ meetup of 2023](https://www.meetup.com/gitlabanz/events/290508374/?utm_budget=cmty)

[Slides](https://docs.google.com/presentation/d/e/2PACX-1vSy5Bdz5MrddZOrfzwAvi3pV7QO1QLfnZ-TEvoUd8btlEtr7Ndoniok4kQKBn34sgyKmAuag4-DC9Jm/pub?start=false&loop=false&delayms=3000)